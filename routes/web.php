<?php
$router->get('/', function () use ($router) {
    //return $router->app->version();
    return "<h1>Backend Test</h1>";
});

$router->group(['prefix'=>'api'],function () use ($router){
    $router->post('register','AuthController@register');
    $router->post('login','AuthController@login');
    $router->get('hash','AuthController@Hash');
});



