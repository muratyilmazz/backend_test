<?php

namespace App\Http\Controllers;

use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    //
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function Register(Request $request){
        $this->validate($request,[
             'name'=>'required|string',
             'email'=>'required|email',
             'password'=>'required|confirmed'
        ]);

        try {

            $user = new User;
//            $user->id-=$request->input('id');
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);

            $user->save();

            //return successful response
            $credentials =$request->only(['email','password']);
            if(!$token=Auth::attempt($credentials)){
                return response()->json(['message'=>'Unauthorized'],401);

            }
            return $this->respondWithToken($token);


        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => $e], 409);
        }
    }

    public function  login(Request $request){
        $this->validate($request,[
            'email'=>'required|email|string',
            'password'=>'required|string'
        ]);
        $credentials =$request->only(['email','password']);
        if(!$token=Auth::attempt($credentials)){
            return response()->json(['message'=>'Unauthorized'],401);

        }
        return $this->respondWithToken($token);
    }

    public function Hash(Request $request){
        $this->validate($request,[
            'name'=>'required|string',
            'email'=>'required|email'
        ]);
        try{
            $user=new User();
            $name=$user->name = $request->input('name');
            $email=$user->email = $request->input('email');

            $password=DB::table('users')->where('name',$name)->
            where('email',$email)->value('password');
            return response()->json(['hash' =>$password ], 201);

        }catch (\Exception $e){
            return response()->json(['message'=>$e],404);
        }

        /*try{
             $user=User::all($id);
             return response()->json(['user'=>$user],200);
        }catch (\Exception $exception)
        {
            return response()->json(['message'=>$exception],404);
        }*/
    }
}
